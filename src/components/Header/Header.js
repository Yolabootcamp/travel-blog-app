import React from "react";
import { NavLink } from "react-router-dom";
import travel from "../../assests/travel.png";
export default function Header() {
  return (
    <>
      <header className="header-container">
        <h1>Travel Blog.</h1>

        <div className="header-img">
          <img src={travel} alt="Travel blog" />

          {/* <div className="header-content"> */}
          <h3>The sky is the limit.</h3>
        </div>
      </header>
      <div>
        <nav>
          <NavLink to="/" className='nav-icon'>Home</NavLink>
          <NavLink to="/About" className='nav-icon'>About</NavLink>
          <NavLink to="/ContactUs" className='nav-icon'>Contact us</NavLink>
          <NavLink to="/Help" className='nav-icon'>Help</NavLink>
        </nav>
      </div>
    </>
  );
}
