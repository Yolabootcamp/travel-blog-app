import React, { useState } from "react";

export default function ContactUs() {
  const [text, setText] = useState("");

  const [emailText, setEmailText] = useState("");

  const [message, setMessage] = useState("");

  const [submitButton, setSubmitButton] = useState(false);

  const [sendMessage, setSendMessage] = useState({
    name: "",
    email: "",
    message: "",
  });

  const handleText = (event) => {
    setText(event.target.value);
  };
  const handleEmail = (event) => {
    setEmailText(event.target.value);
  };
  const handleMessage = (event) => {
    setMessage(event.target.value);
  };

  const handleSendMessage = () => {
    setSendMessage({
      name: text,
      email: emailText,
      message: message,
    });
    setSubmitButton(true);
  };

  return (
    <div>
      <div className="inputs-title">
        <h1>Contact us. </h1>
      </div>
      <div className="contact-container">
        <form className="inputs-container">
          <div>
            <label className="input-label">
              Name:
              <input
                type="text"
                name="text"
                value={text.value}
                required
                onChange={handleText}
                className="input-name"
              />
            </label>
          </div>
          <div>
            <label className="input-label">
              Email:
              <input
                type="email"
                name="emailText"
                value={emailText.value}
                onChange={handleEmail}
                required
                className="input-email"
              />
            </label>
          </div>
          <div>
            <div className="input-label-message">
              {" "}
              <label> Message:</label>
            </div>
            <textarea
              type="text"
              name="message"
              value={message.value}
              rows="3"
              cols="0"
              onChange={handleMessage}
              className="input-message"
            />
          </div>

          <button
            type="submit"
            onClick={handleSendMessage}
            className="input-btn"
          >
            Submit
          </button>
          {/* {submitButton === true ? (
            <p>
              Thank you for your message. We are going to getting in touch wuth.
            </p>
          ) : (
            setSubmitButton(true)
          )} 
          https://shibe97.github.io/react-awesome-modal/*/}
        </form>
      </div>
    </div>
  );
}
